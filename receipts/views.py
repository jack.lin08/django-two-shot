from django.shortcuts import render, redirect
from .models import Receipt, Account, ExpenseCategory
from django.contrib.auth.decorators import login_required
from .forms import ReceiptForm, CreateCategoryForm, CreateAccountForm

# Create your views here.


@login_required
def list_receipt(request):
    receipt = Receipt.objects.filter(purchaser=request.user)
    context = {"receipt_list": receipt}
    return render(request, "receipts/receipts.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "form": form,
    }

    return render(request, "receipts/create.html", context)


@login_required
def category_list(request):
    receipt = ExpenseCategory.objects.filter(owner=request.user)
    context = {"category_list": receipt}
    return render(request, "receipts/categories.html", context)


@login_required
def account_list(request):
    receipt = Account.objects.filter(owner=request.user)
    context = {"account_list": receipt}
    return render(request, "receipts/accounts.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = CreateCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        form = CreateCategoryForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create_category.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = CreateAccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = CreateCategoryForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create_account.html", context)
